<?php

namespace Baka\SaaS;

use Baka\Auth\AuthentificationManager as Controller;
use Baka\SaaS\Models\Companies;
use Baka\SaaS\Models\Suscriptions;
use Baka\SaaS\Models\Users;
use Exception;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

abstract class AuthentificationManager extends Controller
{
    protected $successLoginRedirect = '/dashboard';
    protected $successLoginRedirectNoWelcome = '/users/welcome';
    protected $successRegistrationRedirectAction = 'welcome';
    protected $failedRegistrationRedirectAction = 'signup';
    protected $failedActivationRedirectAction = 'notFound';
    protected $urlRequest;

    /**
     * User registration form
     *
     * @return View
     */
    public function signupAction()
    {
        //si existe ya la session de social connect significa que vienes de una cuenta de connect social
        if ($socialConnect = is_array($this->session->get('socialConnect'))) {
            $userSocial = $this->session->get('socialConnect');

            $userProfile = Users::getSocialProfile($userSocial['site']);

            //si esta cuenta ya esta linked te logeamos
            $UserLinkedSources = new UserLinkedSources();
            $UserLinkedSources->existSocialProfile($userProfile, $userSocial['site']);

            $this->view->setVar('userProfile', $userProfile);
            $this->view->setVar('socialConnect', true);
        }

        //token_name(token)
        if ($this->request->isPost()) {
            if ($this->security->checkToken()) {
                $user = new Users();

                $user->email = $this->request->getPost('email', 'email');
                $user->password = ltrim(trim($this->request->getPost('password', 'string')));
                $user->displayname = ltrim(trim($this->request->getPost('displayname', 'string')));
                $companyName = trim($this->request->getPost('company', 'string'));
                // $user->profile_image = $this->request->getPost('profile_image', 'string');

                //Ok let validate user password
                $validation = new Validation();
                $validation->add('password', new PresenceOf(['message' => _('The password is required.')]));
                $validation->add('company', new PresenceOf(['message' => _('The company name is required.')]));
                $validation->add('email', new EmailValidator(['message' => _('The email is not valid.')]));

                $validation->add('password',
                    new StringLength([
                        'min' => 8,
                        'messageMinimum' => _('Password is too short. Minimum 8 characters.'),
                    ])
                );

                //validate this form for password
                $messages = $validation->validate($this->request->getPost());
                if (count($messages)) {
                    foreach ($messages as $message) {
                        $this->flash->error($message);
                    }

                    //por alguna razon el social connect jode la shit -_-
                    $this->view->setVar('userProfile', $user);

                    //error redirect
                    return $this->dispatcher->forward([
                        'action' => $this->failedRegistrationRedirectAction,
                    ]);
                }

                //set language
                $user->language = $this->userData->usingSpanish() ? 'ES' : 'EN';

                //user registration
                try {

                    $user->signup();

                    //si es social connect lo registramos con su red social
                    if ($socialConnect) {
                        $UserLinkedSources = new UserLinkedSources();
                        $UserLinkedSources->associateAccount($user, $userProfile, $userSocial['site']);
                    }

                    //regiseter the company , subscription
                    $company = Companies::register($user, $companyName);

                    //start free trial
                    $subscription = Suscriptions::startFreeTrial($company);

                } catch (Exception $e) {

                    $this->flash->error($e->getMessage());

                    //por alguna razon el social connect jode la shit -_-
                    $this->view->setVar('userProfile', $user);

                    //error redirect
                    return $this->dispatcher->forward([
                        'action' => $this->failedRegistrationRedirectAction,
                    ]);
                }

                //page confirmation
                if ($this->userData->isLoggedIn()) {
                    return $this->dispatcher->forward([
                        'action' => $this->successRegistrationRedirectAction,
                    ]);
                } else {
                    //create a session with the user activation key , to resent the user email if he didnt get it
                    $this->session->set('userRegistrationKey', $user->user_activation_key);
                    $activationUrl = $this->config->application->siteUrl . '/' . $this->router->getControllerName() . '/activate/' . $user->user_activation_key;

                    //user registration send email
                    $this->sendEmail('signup', $user);

                    return $this->response->redirect('/' . $this->router->getControllerName() . '/activate/' . $user->user_activation_key);
                }

            } else {
                $this->flash->error('Token Error');
            }
        }
    }
}
