<?php

namespace Baka\SaaS\Models;

use Baka\Auth\Models\Users;
use Baka\Database\Model;
use Exception;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness;

class Companies extends Model
{

    const DEFAULT_COMPANY = 'DefaulCompany';

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $profile_image;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $website;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $users_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $is_deleted;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

    }

    /**
     * Model validation
     *
     * @return void
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'name',
            new PresenceOf([
                'model' => $this,
                'required' => true,
            ])
        );

        // Unique values
        $validator->add(
            'name',
            new Uniqueness([
                'model' => $this,
                'message' => _('This company already has an account.'),
            ])
        );

        return $this->validate($validator);
    }

    /**
     * Register a company given a user and name
     *
     * @param  Users  $user
     * @param  string $name
     * @return Companies
     */
    public static function register(Users $user, string $name): Companies
    {
        $company = new self();
        $company->name = $name;
        $company->users_id = $user->getId();

        if (!$company->save()) {
            throw new Exception(current($company->getMessages()));
        }

        //asssociate the user to this new company
        $userCompany = new UsersAssociatedCompany();
        $userCompany->users_id = $user->getId();
        $userCompany->company_id = $company->getId();
        $userCompany->user_active = 1;
        $userCompany->user_role = 'Admin';

        if (!$userCompany->save()) {
            throw new Exception(current($userCompany->getMessages()));
        }

        //now thta we setup de company and associated with the user we need to setup this as its default company
        if (!UserConfig::findFirst(['conditions' => 'users_id = ?0 and name = ?1', 'bind' => [$user->getId(), self::DEFAULT_COMPANY]])) {
            $userConfig = new UserConfig();
            $userConfig->users_id = $user->getId();
            $userConfig->name = self::DEFAULT_COMPANY;
            $userConfig->value = $company->getId();

            if (!$userConfig->save()) {
                throw new Exception(current($userConfig->getMessages()));
            }
        }

        return $company;
    }

    /**
     * Get the default company the users has selected
     *
     * @param  Users  $user
     * @return Companies
     */
    public static function getDefaultByUser(Users $user): Companies
    {
        //verify the user has a default company
        $defaultCompany = UserConfig::findFirst([
            'conditions' => 'users_id = ?0 and name = ?1',
            'bind' => [$user->getId(), self::DEFAULT_COMPANY],
        ]);

        //found it
        if ($defaultCompany) {
            return self::findFirst($defaultCompany->value);
        }

        //second try
        $defaultCompany = UsersAssociatedCompany::findFirst([
            'conditions' => 'users_id = ?0 and user_active =?1',
            'bind' => [$user->getId(), 1],
        ]);

        if ($defaultCompany) {
            return self::findFirst($defaultCompany->company_id);
        }

        throw new Exception(_("User doesn't have an active company"));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'companies';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Companies[]|Companies
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Companies
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
