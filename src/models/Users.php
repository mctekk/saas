<?php

namespace Baka\SaaS\Models;

use Baka\Hmac\Models\Keys as Api;
use Exception;

class Users extends \Baka\Auth\Models\Users
{
    /**
     *  default company
     * @var Companies
     */
    protected static $company;

    /**
     * Initilize the model
     * @return void
     */
    public function initialize()
    {
        $this->hasManyToMany(
            "id",
            "\Baka\SaaS\Models\UsersAssociatedCompany",
            "users_id", "company_id",
            "\Baka\SaaS\Models\Companies",
            "id",
            ['alias' => 'companies']
        );
    }

    /**
     * Get the current company set for the given request
     *
     * @return Companies
     */
    public function getCurrentCompany(): Companies
    {
        if (!self::$company) {
            throw new Exception(_("No default company set"));
        }

        return self::$company;
    }

    /**
     * Set the company for the given request, given a api key or taking it from its default value
     *
     * @param string $key
     * @return Companies
     */
    public function setCompany(string $key)
    {
        //didnt sent a default company
        if (empty($key)) {
            if (!$default = Companies::findFirst($this->config()->get(Companies::DEFAULT_COMPANY))) {
                throw new Exception(_("User doesnt have a default company"));
            }
        } else {
            //we sent the company we are looking for, so lets get iT
            if ($api = Api::findFirst(['conditions' => 'users_id = ?0 and public = ?1', 'bind' => [$this->getId(), $key]])) {
                //$default = $this->getCompanies($api->company_id)[0];
                $default = Companies::findFirst($api->company_id);
            } else {
                throw new Exception(_("User doesnt have a company assigned to it"));
            }
        }

        if (!$default) {
            throw new Exception(_("How did you get here? someone added this user manualy -_- "));
        }

        self::$company = $default;
    }
}
